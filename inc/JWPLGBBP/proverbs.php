<?php namespace JWPLGBBP;
/*
File: proverbs.php
Path: {wp_plugin_dir}/bible-plus-pro/inc/JWPLGBBP/proverbs.php
Created: 01.14.17 @ 08:07 EST
Modified: 01.16.17 @ 14:34 EST
Author: Joshua Wieczorek
---
Description: Daily Proverb controller for
the this plugin.
*/

class Proverbs
{
    // Local Bible Class object
    private $bible;
    // Current day
    private $_day;
    // Verse of the day json
    private $_vod;
    // Shortcode atts
    private $_atts;

    /**
     * Sets the bible, day and vod variables.
     * @param Bible $bible (bible class which from to get the proverb)
     */
    function __construct(Bible $bible)
    {
        // Set bible
        $this->bible    = $bible;
        // Set current day
        $this->_day     = (int) date("d", current_time('timestamp'));
        // Verse of the day option
        $this->_vod     = get_option('jwplgbbp_vods');

    }

    /**
    * Sets settings upon plugin initialization
    */
    public function init()
    {
        // If option is not in database
        if( !$this->_vod && $json=file_get_contents(jwplgbbp_fcreate_path(JWPLGBBP_PATH.'assets/json/vods.json'))) :
            // Update option in database
            update_option('jwplgbbp_vods' , json_decode($json,1));
        // End if option
        endif;
    }

    /**
     * Renders the daily proverb verse.
     * @param  array $atts (attributes passed by shortcode)
     * @return string       (html daily verse)
     */
    public function shortcode($atts)
    {
        // Shortcode attribute defaults
        $attributes = shortcode_atts( array(
            'passage'       => 'Pr'.$this->_day.':'.$this->_vod[$this->_day],
            'version'       => 'KJV',
            'cnum'          => 'no',
            'vnum'	        => 'no',
            'vpl'	        => 'yes'
        ), $atts );
        // Set attributes to class
        $this->_atts = $attributes;
        // Set attributes
        $this->bible->set_atts($attributes);
        // Set passage
        $this->bible->set_passage($attributes['passage']);
        // Set version
        $this->bible->set_version($attributes['version']);
        // Open Html
        $html  = $this->bible->render($this->_passage_reference());
        // Return verse
        return $html;
    }

    /**
     * Renders the daily proverb chapter.
     * @param  array $atts (attributes passed by shortcode)
     * @return string       (html daily chapter)
     */
    public function shortcode_chapter($atts)
    {
        // Shortcode attribute defaults
        $attributes = shortcode_atts( array(
            'passage'       => $this->_create_passage_reference('chapter'),
            'version'       => 'KJV',
            'cnum'          => 'yes',
            'vnum'	    => 'yes',
            'vpl'	    => 'yes'
        ), $atts );
        // Set attributes
        $this->bible->set_atts($attributes);
        // Set passage
        $this->bible->set_passage($attributes['passage']);
        // Set version
        $this->bible->set_version($attributes['version']);
        // Return chapter
        return $this->bible->render();
    }

    /**
     * Creates the html for list of chapters
     * @return string (html list of chapters)
     */
    public function shortcode_chapter_list()
    {
        // Return chapter list
        return $this->_render_chapter_list();
    }

    /**
     * Add reference to passage
     * @return string (html string with reference)
     */
    private function _passage_reference()
    {
        // Return reference
        return '<div class="bible-plus-passage-reference">'.__('Proverbs ', 'jwplgbbp').$this->_day.' : '.$this->_vod[$this->_day].' <span class="bible-plus-reference-version">'.strtoupper($this->_atts['version']).'</span>'.'</div>';
    }

    /**
     * Returns the chapter number based on the day or url.
     * @return int (chapter number)
     */
    private function _get_chapter_number()
    {
        // Get chapter from query var
        $chapter = filter_input(INPUT_GET, 'proverbs-chapter', FILTER_SANITIZE_NUMBER_INT);
        // Return day
        return ($chapter && ($chapter > 0 && $chapter < 32)) ? (int) $chapter : (int) $this->_day;
    }

    /**
     * Creates passage reference.
     * @param  string $type (verse/chapter)
     * @return string       (passage reference)
     */
    private function _create_passage_reference($type)
    {
        // If type is verse
        if( $type=='verse' ) :
            return 'Pr'.$this->_day.':'.$this->_vod[$this->_day];
        // If type is chapter
        elseif($type=='chapter') :
            return 'Pr'.$this->_get_chapter_number();
        // End if verse
        endif;
    }

    /**
     * Creates the list of chapter links.
     * @return string (html links)
     */
    private function _render_chapter_list()
    {
        // Open html
        $html = '<div class="jwplg-bbp-daily-chapter-list"><ul class="jwplg-bbp-daily-chapter-list-ul">';
        // Open li tag for today
        $html .= '<li class="jwplg-bbp-daily-chapter-list-item-today">';
        // A tag
        $html .= '<a href="'.add_query_arg('proverbs-chapter', $this->_day).'">'.__('Today', 'jwplgbbp').'</a>';
        // Close li tag for today
        $html .= '</li>';
        // Iterator
        $i = 1;
        // Create html links
        while($i<=31) :
            // Open li tag
            $html .= '<li class="jwplg-bbp-daily-chapter-list-item">';
            // A tag
            $html .= '<a href="'.add_query_arg('proverbs-chapter', $i).'">'.__('Proverbs '.$i, 'jwplgbbp').'</a>';
            // Close li tag
            $html .= '</li>';
            // Increase iterator
            ++$i;
        endwhile;
        // Close html
        $html .= '</ul></div>';
        // Return html
        return $html;
    }
}
